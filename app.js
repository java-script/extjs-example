Ext.define('Cookbook.Vehicle', {
  config: {
    Manufacturer: 'Aston Martin',
    Model: 'Vanquish',
    topSpeed: 0,
  },

	constructor: function(manufacturer, model, topSpeed){
	    // initialise our config object
	    this.initConfig();
	
	    if (manufacturer){
	    	this.setManufacturer(manufacturer);
	    }
	    
	    if(model){
	    this.setModel(model);
	    }
	    
	    if(topSpeed){
	    this.setTopSpeed(topSpeed);
	    }	
	},
  
  getDetails: function(){
     alert('I am an ' + this.getManufacturer() + ' ' + this.
     getModel());
  },

  applyManufacturer: function(manufacturer){
    Ext.get('manufacturer').update(manufacturer);
    return manufacturer;
  },

  applyModel: function(model){
    Ext.get('model').update(model);
    return model;
  },
  
  travel: function(distance){
	  alert('The ' + this.getManufacturer() + ' ' + this.
	  getModel() + ' travelled ' + distance + ' miles at ' + this.
	  getTopSpeed() + 'mph');
	  },
  
}, function(){
	console.log('Vehicle Class defined!');
	
});

Ext.define('Cookbook.Plane', {
	extend: 'Cookbook.Vehicle',
	
	config: {
		maxAltitude: 0
	},

	constructor: function(manufacturer, model, topSpeed, maxAltitude){
		// initialise our config object
		this.initConfig();
		if(maxAltitude){
			this.setMaxAltitude(maxAltitude);
		}
		// call the parent class' constructor
		this.callParent([manufacturer, model, topSpeed]);
	},
	
	takeOff: function(){
		alert('The ' + this.getManufacturer() + ' ' + this.getModel()
		+ ' is taking off.');
		},
		
	land: function(){
		alert('The ' + this.getManufacturer() + ' ' + this.getModel()
		+ ' is landing.');
		},
		
		travel: function(distance){
			this.takeOff();
			// execute the base class� generic travel method
			this.callParent(arguments);
			alert('The ' + this.getManufacturer() + ' ' + this.getModel()
					+ ' flew at an altitude of ' + this.getMaxAltitude() + 'feet');
					this.land();
		}
		
}, function(){
	console.log('Plane Class Defined!');
});


var plane = Ext.create('Cookbook.Plane', 'Boeing', '747', 500, 30000);
plane.travel(800);